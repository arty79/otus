Сборка образа вручную
docker build -t java:1.0 .
docker run -d -p 8000:8000 java:1.0

Команда запуска 

skaffold run 

при наличии плагина в системе, иначе

kubectl apply -f .

curl http://arch.homework/otusapp/apanasyuk/health - команда для проверки работоспособности в миникубе

Для проверки в браузере

после вызова  kubectl get ingress hello-ingress получить IP сервиса

и прописать его в etc/hosts  - например так 172.17.206.41  arch.homework

http://arch.homework/otusapp/apanasyuk/health - эндпоинт для проверки работоспособности в браузере



